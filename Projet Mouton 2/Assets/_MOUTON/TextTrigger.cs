﻿using UnityEngine;
using System.Collections;

public class TextTrigger : MonoBehaviour {

	Animator ator;

	// Use this for initialization
	void Start () {
		ator = GetComponentInChildren<Animator>();
	}
	
	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Player") 
			ator.Play ("FadeIn");
		
	}

	void OnTriggerExit(Collider col)
	{
		if(col.tag == "Player")
			ator.Play("FadeOut");
	}
}
